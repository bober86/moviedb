## Demo
[Heroku](https://docker-moviesdb.herokuapp.com/)


## QuickStart
Before start you need set two variables in .env file.
First is variable with connection string to mongoDB `MONGODB_URI_LOCAL` in dev mode or `MONGODB_URI` in prod mode
```
    MONGODB_URI_LOCAL="mongodb://localhost:27027/movieDB"
```

Second variable is api key to https://www.omdbapi.com/
```
    API_KEY=xxxxx
```


If you do not have own mongoDB database you can run mongo locally just run

```bash
$ docker-compose -f docker-compose-db.yml up
```
database will be available on 27027 port

### Start the project

```bash
$ npm i
$ npm run build
$ npm run start
```

If you don't want install any dependency on your computer you can use docker to run project

```bash
$ docker-compose up
```
Remember to set correctly variables in docker-compose.yml file

```$xslt
    environment:
      MONGODB_URI: "mongodb://localhost:27017/movieDB"
      API_KEY: "example_API_KEY"
```
### Npm Scripts

- Use `npm run lint` to check code style
- Use `npm test` to run unit test
- se `npm run build` to compiled project from ts to js

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:4000/
```
### Documentation
Documentation (swagger) is available on url:
```
http://localhost:4000/docs
```

### Requirement

- [Node.js v12.14.1](https://nodejs.org/en/)
- [Typescript 3.7+](https://www.typescriptlang.org/index.html#download-links)
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/)
