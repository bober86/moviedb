import request from 'supertest';
import app from '../src/app';
import mongoose from 'mongoose';

export function post(url, body){
    const httpRequest = request(app).post(url);
    httpRequest.send(body);
    httpRequest.set('Accept', 'application/json');
    httpRequest.set('Origin', 'http://localhost:4000');
    return httpRequest;
}

beforeAll(async () => {
    await mongoose.connect(process.env.MONGO_URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true }, (err) => {
        if (err) {
            console.error(err);
            process.exit(1);
        }
    });
});

describe('GET /api/movies', function () {
    it('Get Movie from empty db', async () => {
        const response =  await request(app).get('/api/movies');
        expect(response.status).toBe(200);
        expect(response.body.result).toHaveLength(0);
    });
});

describe('GET /api/comments', () => {
    it('Get comments from empty db', async () => {
        const response =  await request(app).get('/api/comments');
        expect(response.status).toBe(200);
        expect(response.body.result).toHaveLength(0);
    });
});

describe('POST /api/movies', () => {
    it('Add Movie', async () => {
        const title = 'Batman';
        const response =  await post('/api/movies', { title });
        expect(response.status).toBe(201);
        expect(response.body.status).toBe('success');
        expect(response.body.result.title).toBe(title);

    });
    it('Get Movie', async () => {
        const response =  await request(app).get('/api/movies');
        expect(response.status).toBe(200);
        expect(response.body.result).toHaveLength(1);
    });
});

describe('GET /api/movies/:imdbID', () => {
    it('respond with idisnonexisting', async () => {

        const response =  await request(app).get('/api/movies/idisnonexisting');
        expect(response.header['content-type']).toBe('application/json; charset=utf-8');
        expect(response.status).toBe(404);
        expect(response.body.status).toBe('error');

    });
    it('respond with exist imdbID', async () => {
            const imdbID = 'tt0096895';
            const response = await request(app).get(`/api/movies/${imdbID}`);
            expect(response.header['content-type']).toBe('application/json; charset=utf-8');
            expect(response.status).toBe(200);
            expect(response.body.status).toBe('success');
            expect(response.body.result.imdbID).toBe(imdbID);
            expect(response.body.result.comments).toHaveLength(0);

    });


});

describe('POST /api/comments', () => {
    it('add comment to movie', async () => {
        const imdbID = 'tt0096895';
        const comment = 'Good movie';
        const response =  await post('/api/comments', { imdbID, comment });
        expect(response.status).toBe(200);
        expect(response.body.status).toBe('success');
        expect(response.body.result.imdbID).toBe(imdbID);
        expect(response.body.result.comments).toHaveLength(1);
    });

    it('add comment to movie', async () => {
        const imdbID = 'tt0096895';
        const error = [{
                msg: 'comment is required\'s',
                param: 'comment',
                location: 'body'
            }];
        const response =  await post('/api/comments', { imdbID });
        expect(response.status).toBe(422);
        expect(response.body.status).toBe('error');
        expect(response.body.result).toHaveLength(1);
        expect(response.body.result).toEqual(expect.arrayContaining(error));
    });
});


