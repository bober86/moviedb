import request from 'supertest';
import app from '../src/app';


describe('GET /api/movies', () =>{
    it('GET /', async () => {
        const response = await request(app).get('/');
        expect(response.status).toBe(200);
        expect(response.body.status).toBe('success');
        expect(response.body.result).toBe('Api is working');
    });
});
