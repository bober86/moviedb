import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { Response, Request } from 'express';
import mainRoutes from './routes/index';
import bluebird from 'bluebird';
import compression from 'compression';
import helmet from 'helmet';
import mongoose from 'mongoose';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json';

import { MONGO_URL } from './util/secrets';
import { createResponse, statusType } from './util/response';

const mongoUrl = MONGO_URL;
mongoose.Promise = bluebird;
mongoose.set('useFindAndModify', false);
mongoose.connect(mongoUrl, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true } ).then(
    () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ },
).catch(err => {
    console.log('MongoDB connection error. Please make sure MongoDB is running. ' + err);
    // process.exit();
});

const app = express();

app.use(compression());
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('port', process.env.PORT || 4000);

app.get('/', (req: Request, res: Response) => res.json(createResponse(statusType.success, 'Api is working')));
app.use('/api', mainRoutes);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

export default app;

