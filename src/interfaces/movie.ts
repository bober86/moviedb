export interface IMovie {
    _id?: string;
    title: string;
    year: string;
    rated: string;
    released: Date;

    runtime: string;
    genre: string;
    director: string;
    writer: string;

    actors: string;
    plot: string;
    language: string;
    country: string;
    awards: string;
    poster: string;
    imdbID: string;
}

