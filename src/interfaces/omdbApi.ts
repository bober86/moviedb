export interface IMovieOmdb {
    Title: string;
    Year: string;
    Rated: string;
    Released: string;
    Runtime: string;
    Genre: string;
    Director: string;
    Writer: string;
    Actors: string;
    Plot: string;
    Country: string;
    Awards: string;
    Poster: string;
    Ratings: IMovieOmdbRating[];
    Metascore: string;
    imdbRating: string;
    imdbVotes: string;
    imdbID: string;
    Type: string;
    DVD: string;
    Language: string;
    BoxOffice: string;
    Production: string;
    Website: string;
    Response: string;
}

interface IMovieOmdbRating {
    Source: string;
    Value: string;
}


