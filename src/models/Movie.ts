import mongoose from 'mongoose';
import { IMovie } from '../interfaces/movie';

export type MoveType = mongoose.Document & IMovie;
const toJSON = {
    transform: (doc: any, obj: any) => {
        delete obj.__v;
        delete obj._id;
        return obj;
    }
};
const movieSchema = new mongoose.Schema({
    title: { type: String },
    imdbID: { type: String },
    year: String,
    rated: String,
    released: Date,

    runtime: String,
    genre: String,
    director: String,
    writer: String,

    actors: String,
    plot: String,
    language: String,
    country: String,
    poster: String,
    awards: String,
    comments: [{
        content : String,
        date : Date
    }]

}, { timestamps: true, toJSON});

export const Movie = mongoose.model<MoveType>('Movie', movieSchema);

export const addComment =  (imdbID: string, comment: string) => {
    return Movie.findOneAndUpdate({ imdbID },
        {$push: {comments:
                    Object.assign({}, {content: comment}, {date: new Date()})
            }
        },
        { new: true }
    );
};

export const findByImdbID =  (imdbID: string) => {
    return Movie.findOne({ imdbID });
};
