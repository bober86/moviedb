import { Request, Response } from 'express';
import { addComment, Movie } from '../models/Movie';
import { validationResult } from 'express-validator';
import { createResponse, statusType } from '../util/response';

export const getComments = async (req: Request, res: Response) => {

    const result = await Movie.find({ }).select('imdbID title comments');

    res.json(createResponse(statusType.success, result));
};

export const postComments = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json(createResponse(statusType.error, errors.array()));
        return;
    }
    const { imdbID, comment } = req.body;
    const result = await addComment(imdbID, comment);
    res.json(createResponse(statusType.success, result.toJSON()));
};
