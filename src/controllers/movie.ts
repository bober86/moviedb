import { Request, Response } from 'express';
import { RequestOmdb } from '../util/OmdbApi';
import { addComment, findByImdbID, Movie } from '../models/Movie';
import { validationResult } from 'express-validator';
import { createResponse, statusType } from '../util/response';


export const postMovie = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json(createResponse(statusType.error, errors.array()));
        return;
    }
    const movieAPI = await new RequestOmdb().find(req.body.title);
    let result = await findByImdbID(movieAPI.imdbID);
    if(!!!result) {
        result = await new Movie(movieAPI).save();
    }
    res.status(201);
    res.json(createResponse(statusType.success, result.toJSON()));

};
export const getMovie = async (req: Request, res: Response) => {
    const { imdbID } = req.params;
    const result = await findByImdbID(imdbID);
    if(!result) {
        res.status(404);
        res.json(createResponse(statusType.error, 'Not Found'));
        return;
    }
    res.json(createResponse(statusType.success, result.toJSON()));

};


export const getAllMovies = async (req: Request, res: Response) => {
    const result = await Movie.find({}).select('-__v -_id');
    res.json({status: 'success', result});
};


export const postComment = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json(createResponse(statusType.error, errors.array()));
        return;
    }
    const { imdbID } = req.params;

    const result = await addComment(imdbID, req.body.comment);
    if(!result) {
        res.status(404);
        res.json(createResponse(statusType.error, 'Not Found'));
        return;
    }
    res.json({status: 'success', result});

};

export const getComment = async (req: Request, res: Response) => {

    const { imdbID } = req.params;
    const result = await Movie.findOne({ imdbID }).select('comments');
    if(!result) {
        res.status(404);
        res.json(createResponse(statusType.error, 'Not Found'));
        return;
    }
    res.json(createResponse(statusType.success, result));
};
