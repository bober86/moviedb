import { body } from 'express-validator';

const validate = (method: string) => {
    switch (method) {
        case 'postComment': {
            return [
                body('comment', 'comment is required\'s').exists(),
                body('imdbID', 'imdbID is required\'s').exists(),
            ];
        }
    }
};

export default validate;
