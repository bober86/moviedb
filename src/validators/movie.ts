import { body } from 'express-validator';

const validate = (method: string) => {
    switch (method) {
        case 'postMovie': {
            return [
                body('title', 'title is required\'s').exists().bail(),
            ];
        }
        case 'postComment': {
            return [
                body('comment', 'comment is required\'s').exists().bail(),
            ];
        }
    }
};

export default validate;
