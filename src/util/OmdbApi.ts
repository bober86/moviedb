import request from 'request';
import { Response } from 'request';
import { IMovieOmdb } from '../interfaces/omdbApi';
import { API_KEY } from './secrets';
import { IMovie } from '../interfaces/movie';


export class RequestOmdb {
    protected endpoint = 'https://www.omdbapi.com/';
    protected apikey: string = API_KEY;

    // protected toLowerCaseKeys(obj) {
    //     return Object.keys(obj).reduce((c, k) => (c[k.toLowerCase()] = obj[k], c), {});
    // }
    //
    // protected objectKeysToLowerCase(origObj: object) {
    //     return (Object.keys(origObj) as Array<keyof typeof origObj>).reduce( (newObj, key) => {
    //         let val = origObj[key];
    //         let newVal = (typeof val === 'object') ? this.objectKeysToLowerCase(val) : val;
    //         newObj[key.toLowerCase()] = newVal;
    //         return newObj;
    //     }, {} as object);
    // }
    transformRequest(data: IMovieOmdb) {
       return  {
            title: data.Title,
            year: data.Year,
            rated: data.Rated,
            released: new Date(data.Released),
            runtime: data.Runtime,
            genre: data.Genre,
            director: data.Director,
            writer: data.Writer,
            actors: data.Actors,
            plot: data.Plot,
            country: data.Country,
            awards: data.Awards,
            poster: data.Poster,
            imdbID: data.imdbID,
            language: data.Language,

        };
    }
    public sendRequest(data: object): Promise<IMovie> {


            const reqOptions: any = {
                url: this.endpoint,
                method: 'GET',
                qs: Object.assign({}, data,{ apikey: this.apikey })
            };
            return new Promise((resolve, reject) => {
                request.post(reqOptions, (err: any, response: Response, body: any) => {
                    if (err || response.statusCode !== 200) {
                        return reject(err);
                    }
                    try {
                        return resolve(this.transformRequest(JSON.parse(body)));
                    } catch (e) {
                        return reject(`Error when parsing string to JSON: ${e} `);
                    }

                });
            });
    }

    public find(t: string) {
        return this.sendRequest({ t });
    }

}
