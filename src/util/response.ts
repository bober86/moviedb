export const createResponse = (status: string, result: any) => ({ status, result });
export const statusType = {
    success: 'success',
    error: 'error',
};
