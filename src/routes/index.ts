import express from 'express';
import movieRoutes from './movie';
import commentRoutes from './comment';
const router = express.Router();


router.use('/movies', movieRoutes);
router.use('/comments', commentRoutes);

// Export the router
export default router;
