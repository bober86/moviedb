import express from 'express';
import * as commentController from '../controllers/comment';
import validate from '../validators/comment';


const commentRoutes = express.Router();

commentRoutes.get('/', commentController.getComments);
commentRoutes.post('/', validate('postComment'), commentController.postComments);



export default commentRoutes;
