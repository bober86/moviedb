import express from 'express';
import * as movieController from '../controllers/movie';
import validate from '../validators/movie';



const movieRoutes = express.Router();

movieRoutes.post('/', validate('postMovie'), movieController.postMovie);
movieRoutes.get('/', movieController.getAllMovies);
movieRoutes.get('/:imdbID', movieController.getMovie);
movieRoutes.post('/:imdbID/comments', validate('postComment'), movieController.postComment);
movieRoutes.get('/:imdbID/comments', movieController.getComment);

export default movieRoutes;
